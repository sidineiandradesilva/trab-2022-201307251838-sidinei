# ##
# 1) Foi criada a variaver sexo como opção de F ou M para exibição das mensagem
# 2) Foi usando a condição IF para exibição das mensagem. 
# ##
sexo = input("Qual o seu sexo? Masculino (M) ou Feminino (F)?")
altura = float(input("Digite a sua altura: "))

peso_ideal_m = (72.7 * float(altura)) - 58
peso_ideal_f = (62.1 * float(altura)) - 44.7

if sexo == "M":
    print("O seu peso ideal é %.2f: " % peso_ideal_m)
elif sexo == "F":
    print("O seu peso ideal é %.2f: " % peso_ideal_f)
else:
    print("O sexo digitado é invalido!")