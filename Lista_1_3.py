# ##
#  1) Foi a variavel de recebimento de valor do salario 
#  2) Ja inicializer as varias de percentuais(perc_ir, perc_inss, perc_sindicato) 
#  3) Já definir os valores dos impostos
#  4) Na exibição do valores de impostos, liquido e tambem o desconto.  
# # ##

valor_Salario_bruto = float(input("Informar o Salario Bruto : R$ "))  
perc_ir = 0.11
perc_inss = 0.08
perc_sindicato = 0.05
valor_salario_liquido = 0

valor_ir = valor_Salario_bruto * perc_ir
valor_inss  =  valor_Salario_bruto * perc_inss
valor_sindicato = valor_Salario_bruto * perc_sindicato

valor_salario_liquido = valor_Salario_bruto - valor_ir -  valor_inss - valor_sindicato

vlr_total_desconto = valor_Salario_bruto - valor_salario_liquido

print("Valor do IR  %.2f" % valor_ir)  
print("Valor do INSS %.2f" % valor_inss)  
print("Valor do Sindicato %.2f\n" % valor_sindicato)  
print("Total de Desconto(s) %.2f" % vlr_total_desconto )
print("Salário Liquido: %.2f" % valor_salario_liquido)  