# ##
#  1) Defini valor do litros / 6 arredondo para cima, e definido um valor inteiro.
#  2) Definir o quantidade e valor de latas.
#  3) Definir o quantidade e valor de galoes.
#  4) Definir o quantidade e valor de mistura_lata.
#  5) Exibir os resultado. 
# ##

import math

metros = float(input("Informe os m²: "))
litro = math.ceil(metros/6)

latas = litro / 18
if latas % 18 != 0:
  latas += 1
  preco_das_latas = latas * 80

# GALOES
galoes = litro / 3.6
if galoes % 3.6 != 0:
  galoes += 1
  preco_dos_galoes = galoes * 25


mistura_lata = int(litro / 18.0)
mistura_galao = int((litro - (mistura_lata * 18)) / 3.6)
if litro - (mistura_lata * 18) % 3.6 != 0:
  mistura_galao += 1
  galoes_total = (mistura_lata * 80) + (mistura_galao * 25)

print(f"\nTotal de Litros necessário para pintar: {litro:.0f}L \n")


print(f"Apenas latas de 18 litros: {latas:.0f} Und. - Preço: R$ {preco_das_latas:.2f}\n")


print(f"Apenas galões de 3.6 litros: {galoes:.0f} Und. - Preço: R$ {preco_dos_galoes:.2f} \n")

print(f"Mistura: {mistura_lata:.0f} Latas e {mistura_galao:.0f} Galões = R$ {galoes_total:.2f}")     
       