# ##
# Ao digitar é verificado os numero tem resto, se tiver é decimal , se não é interio pois o resto é igual 0.  
# ##
numero = input("Digite um numero: ")
isId = (float(numero) % 1)
if isId == 0: 
  print(f"O numero {numero.strip()} é Inteiro.")
else:
  print(f"O numero {numero.strip()} é Decimal.")