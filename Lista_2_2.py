# ##
# Ao digitar é verificado os numero tem resto, se tiver é Impar , se não é Par pois o resto é igual 0.  
# ##
numero = input("Digite o numero: ")
isPar = (int(numero) % 2)
if isPar == 0: 
  print(f"O numero {numero.strip()} é Par.")
else:
  print(f"O numero {numero.strip()} é Impar.")