# ##
# 1) Definir os Array idades e alturas que são aculmuladores de informação
# 2) A cada passo preencher as variaveis e adicionando nos arrays
# 3) Criando a Order Inversa e Lida. 
# ##

idades = []
alturas = []
for i in range(1, 5):
    print('%dº Pessoa' %i)
    idade = int(input('Digite a idade: '))
    altura = float(input('Digite a altura: '))
    idades.append(idade)
    alturas.append(altura)

print('Ordem inversa')
print('Alturas')
print(alturas[::-1])
print('Idades')
print(idades[::-1])

print('Ordem lida')
print('Alturas')
print(alturas)
print('Idades')
print(idades)